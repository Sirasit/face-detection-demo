package com.example.facedetectiondemo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    private Button mButtonSignUp,mButtonSignIn;
    private EditText mEditTextEmail,mEditTextPassword;
    private String email,password;
    private ProgressDialog mProgressDialog;
    private FirebaseAuth mFirebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButtonSignUp = (Button)findViewById(R.id.buttonSignUp);
        mButtonSignIn = (Button)findViewById(R.id.buttonSignIn);

        mEditTextEmail = (EditText)findViewById(R.id.editTextEmail);
        mEditTextPassword = (EditText)findViewById(R.id.editTextPassword);

        mProgressDialog = new ProgressDialog(this);

        //Get FirebaseAuth Instance
        mFirebaseAuth = FirebaseAuth.getInstance();

        // Set On Click Listener

        mButtonSignUp.setOnClickListener(this);
        mButtonSignIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        if (view == mButtonSignUp)
        {
            // Register new User
            registerNewUser();
        }
        else if (view == mButtonSignIn)
        {
            //Login
            userLogin();
        }

    }

    private void registerNewUser()
    {
        email = mEditTextEmail.getText().toString().trim();
        password = mEditTextPassword.getText().toString().trim();

        if (email.isEmpty())
        {
            Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show();
            return;
        }
        if (password.equals(""))
        {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }

        mProgressDialog.setMessage("Registering....");
        mProgressDialog.show();

        mFirebaseAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {
                mProgressDialog.dismiss();
                if (task.isSuccessful())
                {
                    Toast.makeText(MainActivity.this, "Registered Successfully", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(MainActivity.this, "Failed to register, Please try again later", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void userLogin()
    {
        email = mEditTextEmail.getText().toString().trim();
        password = mEditTextPassword.getText().toString().trim();

        if (email.equals(""))
        {
            Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show();
            return;
        }
        if (password.isEmpty())
        {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }

        mProgressDialog.setMessage("Please wait....");
        mProgressDialog.show();

        mFirebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {
                mProgressDialog.dismiss();
                if(task.isSuccessful())
                {
                    finish();
                    startActivity(new Intent(MainActivity.this,ProfileActivity.class));
                }
                else
                {
                    Toast.makeText(MainActivity.this, "Failed to login, Please try again later", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

}
