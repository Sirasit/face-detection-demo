package com.example.facedetectiondemo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.gms.vision.face.Landmark;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    private Button mButtonImport, mButtonOpenCamera, mButtonUpload, mButtonDetect, mButtonRotate;
    private static final int RSQ = 123;
    private ImageView mImageView;
    private Bitmap mBitmap;
    private StorageReference mStorageReference;
    private ProgressDialog mProgressDialog;
    private Uri mUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mButtonImport = (Button) findViewById(R.id.buttonImport);
        mButtonOpenCamera = (Button) findViewById(R.id.buttonOpenCamera);
        mButtonUpload = (Button) findViewById(R.id.buttonUpload);
        mButtonDetect = (Button) findViewById(R.id.buttonDetectFace);
        mButtonRotate = (Button) findViewById(R.id.buttonRotate);



        mImageView = (ImageView) findViewById(R.id.imageViewDisplay);

        mStorageReference = FirebaseStorage.getInstance().getReference();

        mButtonImport.setOnClickListener(this);
        mButtonOpenCamera.setOnClickListener(this);
        mButtonUpload.setOnClickListener(this);
        mButtonDetect.setOnClickListener(this);
        mButtonRotate.setOnClickListener(this);



        mProgressDialog = new ProgressDialog(this);

    }

    @Override
    public void onClick(View view) {
        if (view == mButtonImport) {
            // Import photo from gallery
            importPhoto();
        } else if (view == mButtonOpenCamera)
        {
            //open SurfaceView Camera
        } else if (view == mButtonUpload) {
            //Upload Selected photo to the Firebase
            uploadFile();
        }
        else if (view == mButtonDetect)
        {
            // detect user's Face
            detectFace();
        }
        else if (view == mButtonRotate)
        {
            //rotate the image
            rotateImage();
        }

    }

    private void rotateImage()
    {
        if (mBitmap != null) {
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            mBitmap = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
            mImageView.setImageBitmap(mBitmap);
        }
        else
        {
            Toast.makeText(this, "No photo to rotate", Toast.LENGTH_SHORT).show();
        }
    }
    private void detectFace() {
        if (mBitmap != null) {

            Paint myRectPaint = new Paint();
            myRectPaint.setStrokeWidth(5);
            myRectPaint.setColor(Color.GREEN);
            myRectPaint.setStyle(Paint.Style.STROKE);

            Paint landmarksPaint = new Paint();
            landmarksPaint.setStrokeWidth(10);
            landmarksPaint.setColor(Color.RED);
            landmarksPaint.setStyle(Paint.Style.STROKE);


            Bitmap tempBitmap = Bitmap.createBitmap(mBitmap.getWidth(), mBitmap.getHeight(), Bitmap.Config.RGB_565);
            Canvas tempCanvas = new Canvas(tempBitmap);
            tempCanvas.drawBitmap(mBitmap, 0, 0, null);


            FaceDetector faceDetector =
                    new FaceDetector.Builder(getApplicationContext())
                            .setTrackingEnabled(false)
                            .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                            .build();

            Frame frame = new Frame.Builder().setBitmap(mBitmap).build();
            SparseArray<Face> faces = faceDetector.detect(frame);


            for (int i = 0; i < faces.size(); i++) {
                Face thisFace = faces.valueAt(i);
                float x1 = thisFace.getPosition().x;
                float y1 = thisFace.getPosition().y;
                float x2 = x1 + thisFace.getWidth();
                float y2 = y1 + thisFace.getHeight();
                tempCanvas.drawRoundRect(new RectF(x1, y1, x2, y2), 2, 2, myRectPaint);


                List<Landmark> landmarks = thisFace.getLandmarks();
                for (int l = 0; l < landmarks.size(); l++) {
                    PointF pos = landmarks.get(l).getPosition();
                    tempCanvas.drawPoint(pos.x, pos.y, landmarksPaint);
                    BitmapDrawable drewBitmap = new BitmapDrawable(getResources(),tempBitmap);
                    mImageView.setImageDrawable(drewBitmap);
                }

            }
            Toast.makeText(this, "Done", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this, "No Photo to detect", Toast.LENGTH_SHORT).show();
        }
    }
    private void importPhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, RSQ);
        //test
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RSQ && resultCode == RESULT_OK && data.getData() != null) {
            mUri = data.getData();

            try {
                mBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mUri);
                mImageView.setImageBitmap(mBitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void uploadFile()
    {
        if (mUri!=null)
        {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyMMdd-HHmmss");
            String filename = simpleDateFormat.format(new Date());
            StorageReference tempStorageReference = mStorageReference.child("images"+filename);
            tempStorageReference.putFile(mUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>()
            {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
                {
                    mProgressDialog.dismiss();
                    Toast.makeText(HomeActivity.this, "File Uploaded", Toast.LENGTH_SHORT).show();


                }
            }).addOnFailureListener(new OnFailureListener()
            {
                @Override
                public void onFailure(@NonNull Exception e)
                {
                    mProgressDialog.dismiss();
                    Toast.makeText(HomeActivity.this, "Failed to upload", Toast.LENGTH_SHORT).show();

                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot)
                {
                    double progress = (100.00*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                    mProgressDialog.setMessage("Uploaded : "+((int)progress)+" %");
                    mProgressDialog.show();


                }
            });
        }

    }
}
