package com.example.facedetectiondemo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.w3c.dom.Text;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener
{
    private TextView mTextViewUserEmail;
    private Button mButtonUpdateInfo,mButtonUploadImage,mButtonSignout;
    private FirebaseAuth mFirebaseAuth;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mButtonUpdateInfo = (Button)findViewById(R.id.buttonUpdateInfo);
        mButtonUploadImage = (Button)findViewById(R.id.buttonUploadImage);
        mButtonSignout = (Button)findViewById(R.id.buttonSignout);

        mTextViewUserEmail = (TextView)findViewById(R.id.textViewUserEmail);

        mFirebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mFirebaseAuth.getCurrentUser();
        mTextViewUserEmail.setText(user.getEmail());

        mButtonUpdateInfo.setOnClickListener(this);
        mButtonUploadImage.setOnClickListener(this);
        mButtonSignout.setOnClickListener(this);


    }

    @Override
    public void onClick(View view)
    {
        if (view == mButtonUpdateInfo)
        {
            //Update user's information
            Toast.makeText(this, "Currently not available", Toast.LENGTH_SHORT).show();
        }
        else if (view ==mButtonUploadImage)
        {
            //open Home Activity
            startActivity(new Intent(ProfileActivity.this,HomeActivity.class));

        }
        else if (view == mButtonSignout)
        {
            //sign out
            finish();
            startActivity(new Intent(ProfileActivity.this,MainActivity.class));
        }

    }
}
